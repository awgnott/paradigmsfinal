Andrew Gnott
Nathan Vahrenberg

CSE 30332 Programming Paradigms
Final Project - SuperPong

4 May 2016

Usage:
	Single player (vs AI):
	$ python pong.py

	Multiplayer (network):
	$ python pong.py [player] (hostname) (port)

	To launch SuperPong in multiplayer mode, player 1 should enter "$ python pong.py 1"
	and player 2 should enter "$ python pong.py 2 (hostname) (port)" where the optional
	parameters "hostname" and "port" can be specified to the location of player 1. If
	left blank, hostname will default to "student00.cse.nd.edu" and port will default
	to "40013". Player 1 should launch their game first, as player 1's game acts
	as the server.

Gameplay:
	Once both players are connected, the game begins immediately. The goal of SuperPong
	is the same as the goal of the original Pong: Use your paddle to stop the ball from
	passing your side, and attempt to aim the ball past the opponent's paddle. The paddles
	are controlled with the 'a' and 'd' keys for left-right movement, and will angle
	toward the mouse, allowing the player to aim their shots. Missing the ball awards your
	opponent one point. If either player scores, the ball is automatically served from the
	opponent's side toward the scoring player. After some time, powerups will begin to appear
	in the center of the screen. These powerups will either help the player who hits them,
	hinder the opponent, or affect the ball in some way. Some examples include making the
	player's paddle bigger, slowing down the opponent's movement, or speeding up the ball.
	When the ball connects with a powerup, the last player to hit the ball is awarded the
	powerup, which is automatically used immediately. Once either player earns 11 points,
	they are declared the winner and the game exits.