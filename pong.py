
#Imports
import sys
import os
import pygame
import math
import inspect
import json
import random
import time 

from pygame.locals 							import*

from twisted.internet.protocol 	import ClientFactory
from twisted.internet.protocol 	import Factory
from twisted.internet.protocol 	import Protocol
from twisted.protocols.basic 		import LineReceiver
from twisted.internet.tcp 			import Port
from twisted.internet 					import reactor
from twisted.internet.task 			import LoopingCall

SERVER_HOST = 'student00.cse.nd.edu'
SERVER_PORT = 40013

SCORE_TO_WIN = 11

SOLO_PLAY = False
if len(sys.argv) == 1:
	SOLO_PLAY = True
	PLAYER_INSTANCE = 1
elif len(sys.argv) >= 2:
	PLAYER_INSTANCE = int(sys.argv[1])
	if len(sys.argv) >= 3:
		SERVER_HOST = sys.argv[2]
		if len(sys.argv) >= 4:
			SERVER_PORT = int(sys.argv[3])
else: 
	print 'Error: Usage "python game.py [player] [host] [port]"'
	exit(1)
	
'''
	Event sender class
'''
class EventSender:
	def __init__(self):
		self.dest = None
	def setDestination(self, func):
		self.dest = func
	def sendData(self, data):
		if self.dest:
			self.dest(data)
			
es = EventSender()		
		
'''
	Game client class
'''
class Conn2Server(Protocol):
	def __init__(self,addr,gs):
		self.addr = addr
		self.gs = gs
	def dataReceived(self, data):
		
		#Fix conjoined json objects
		data = data.split('}{')
		
		for i in range(0,len(data)):
			if i < len(data)-1:
				data[i] = data[i] + '}'
			if i > 0:
				data[i] = '{' + data[i]
		
			try: 
				data[i] = json.loads(data[i])
		
				#Updates position of player 1's paddle from other player
				if data[i]['id'] == 'p1':
					for item in self.gs.players:
						if item.player == 1:
							item.angle = data[i]['angle']
							item.normal = data[i]['normal']
							item.rect.center = data[i]['center']
							item.powerup_duration = data[i]['powerup']

				#Updates position of the ball from other player's information
				if data[i]['id'] == 'ball':
					self.gs.ball.invincible = data[i]['invincible']
					self.gs.ball.vel = data[i]['vel']
					self.gs.ball.last_hit = data[i]['last_hit']
					self.gs.ball.rect.center = data[i]['center']
					self.gs.ball.powerup_duration = data[i]['powerup']
				
				#Seeds random number, begins game
				if data[i]['id'] == 'init':
					random.seed(float(data[i]['seed']))
					
					#Start game only after both players are ready
					lc = LoopingCall(self.gs.game_loop_iterate)
					lc.start(1/60.0)
					
				#If the server player added a powerup to the board, add one to this board
				if data[i]['id'] == 'powerup':
					self.gs.objects.append(Powerup(self.gs, data[i]['type'], float(data[i]['posx'])))
					
			except:
				pass
		
	def connectionMade(self):
		print 'Connection made:',self.addr
		
		#Set up event sender
		es.setDestination(self.transport.write)
		
	def connectionLost(self, reason):
		print 'Connection lost:',self.addr
		
		if reactor.running: 
			reactor.stop()

class Conn2ServerFactory(ClientFactory):
	def __init__(self, gs):
		self.gs = gs
	def buildProtocol(self, addr):
		return Conn2Server(addr, self.gs)
		
'''
	Game server class
'''
class Conn2Client(Protocol):
	def __init__(self, addr, gs):
		self.addr = addr
		self.gs = gs
	def dataReceived(self, data):
		
		#Split data on mashed together jsons
		data = data.split('}{')
		
		for i in range(0,len(data)):
			if i < len(data)-1:
				data[i] = data[i] + '}'
			if i > 0:
				data[i] = '{' + data[i]
		
			try: 
				data[i] = json.loads(data[i])
		
				#Update player 2 position from player 2's game state
				if data[i]['id'] == 'p2':
					for item in self.gs.players:
						if item.player == 2:
							item.angle = data[i]['angle']
							item.normal = data[i]['normal']
							item.rect.center = data[i]['center']
							item.powerup_duration = data[i]['powerup']
						
			except: 
				pass
		
	def connectionMade(self):
		print 'Connection made:',self.addr
		
		#Set up event sender
		es.setDestination(self.transport.write)
		
		#Seed random number, send to client
		time_seed = time.clock()
		random.seed(float(time_seed))
		
		data = dict()
		data['id'] = 'init'
		data['seed'] = time_seed
		
		es.sendData(json.dumps(data))
		
		#Begin game after both players are present, if solo, begin countdown
		lc = LoopingCall(self.gs.game_loop_iterate)
		lc.start(1/60.0)
		
	def connectionLost(self, reason):
		print 'Connection lost:',self.addr
		
		if reactor.running: 
			reactor.stop()
			
class Conn2ClientFactory(Factory):
	def __init__(self, gs):
		self.gs = gs
	def buildProtocol(self, addr):
		return Conn2Client(addr, self.gs)
		
'''
	Paddle Class
	- Behavior
			- Move left and right with 'a' and 'd'
			- Rotate to look at mouse
'''
class Paddle(pygame.sprite.Sprite):
	def __init__(self, player, gs=None):
		#General sprite properties
		self.gs = gs
		self.image = pygame.image.load('images/paddle.png')
		self.orig_image = self.image
		self.score = 0
		
		#Localized sprite properties for player, angle, starting position, and normal vector
		self.player = player
		self.angle = 0
		self.vel_offset = 2.5
		if player == 1:
			self.rect = self.image.get_bounding_rect(50)
			self.rect.center = (self.gs.size[0]/2,self.gs.size[1]-50)
			self.angle_offset = -180
		elif player == 2:
			self.rect = self.image.get_bounding_rect(50)
			self.rect.center = (self.gs.size[0]/2,50)
			self.angle_offset = 0
		self.normal = (0,1)
		self.powerup_duration = 0
		
		#History variable to prevent useless connection congestion.
		#Only send information if there has been a change. 
		self.last_angle = self.angle
		self.last_center = self.rect.center
		
	def tick(self):
		
		#Decrement powerup duration if a powerup is equipped
		if self.powerup_duration > 0:
			self.powerup_duration -= 1
		
		#Reset is powerup duration is about to expire
		if self.powerup_duration == 1:
			self.reset()
	
		if self.player == PLAYER_INSTANCE:
	
			#Poll mouse position and keyboard state
			mx,my = pygame.mouse.get_pos()
			keys = pygame.key.get_pressed()
	
			#Handle movement on key presses, moves left and right
			if keys[K_a]:
				self.rect = self.rect.move(-5*self.vel_offset,0)
			if keys[K_d]:
				self.rect = self.rect.move(5*self.vel_offset,0)
	
			#Keep paddles on screen
			
			if self.rect.centerx > self.gs.size[0]:
				self.rect.centerx = self.gs.size[0]
			elif self.rect.centerx < 0:
				self.rect.centerx = 0
	
			#Save rect center, AFTER movement
			rx,ry = self.rect.center
	
			#Calculate angle to rotate the paddle
			self.angle = -math.atan2(my-ry,mx-rx)*180/3.1415+90+self.angle_offset
		
			#Calculate normal vector for collisions
			self.normal = (math.cos((self.angle-90)*3.1415/180.0),-math.sin((self.angle-90)*3.1415/180.0))
	
			#Rotate image, center at new position
			self.image = pygame.transform.rotate(self.orig_image,self.angle)
			self.rect = self.image.get_bounding_rect(50)
			self.rect.center = (rx,ry)
		
			#Send paddle state to other player
			if (self.rect.center != self.last_center or self.last_angle != self.angle) and not SOLO_PLAY:
				self.sendPaddleState()
				
			#Update last variables
			self.last_angle 	= self.angle
			self.last_center 	= self.rect.center 
		
		#AI paddle movement
		elif self.player != PLAYER_INSTANCE and SOLO_PLAY:
			
			#Find distance to the ball from the paddle
			ball_center = self.gs.ball.rect.center
			dist = math.sqrt(
				(self.rect.center[0]-ball_center[0])*(self.rect.center[0]-ball_center[0]) +
				(self.rect.center[1]-ball_center[1])*(self.rect.center[1]-ball_center[1]))
			
			#Move toward ball
			if ball_center[0]+(random.triangular(0,20,10)) < self.rect.center[0] and dist < self.gs.size[0]:
				self.rect = self.rect.move(-4*self.vel_offset,0)
			elif ball_center[0]+(random.triangular(-20,0,-10)) > self.rect.center[0] and dist < self.gs.size[0]:
				self.rect = self.rect.move(4*self.vel_offset,0)
				
			#Calculate angle to rotate the paddle
			if self.angle > 45:
				self.angle = 45
				self.angle += random.triangular(-5,-2,-3)
			elif self.angle < -45:
				self.angle = -45
				self.angle += random.triangular(2,5,3)
			elif self.angle > 30:
				self.angle += random.triangular(-3,1,-1)
			elif self.angle < -30:
				self.angle += random.triangular(-1,3,1)
			else:
				self.angle += random.triangular(-4,4,0)
		
			#Calculate normal vector for collisions
			self.normal = (math.cos((self.angle-90)*3.1415/180.0),-math.sin((self.angle-90)*3.1415/180.0))
					
			#Rotate image, center at new position
			temp = self.rect.center 
			self.image = pygame.transform.rotate(self.orig_image,self.angle)
			self.rect = self.image.get_bounding_rect(50)
			self.rect.center = temp
			
		else:
			temp = self.rect.center
			self.image = pygame.transform.rotate(self.orig_image,self.angle)
			self.rect = self.image.get_bounding_rect(50)
			self.rect.center = temp
		
	def sendPaddleState(self):
		#Create blob to send to client
		data = dict()
		if PLAYER_INSTANCE == 2:
			data['id'] 		= 'p2'
		else:
			data['id']		= 'p1'
		data['angle'] 	= self.angle
		data['normal']	= self.normal
		data['center']	= self.rect.center
		data['powerup'] = self.powerup_duration
		
		es.sendData(json.dumps(data))
	
	def extend(self, duration):
		#Reset for clean powerup slate
		self.reset()
	
		#Load newer, larger image
		(rx,ry) = self.rect.center
		self.image = pygame.image.load('images/paddle_extended.png')
		self.orig_image = self.image
		self.rect = self.image.get_bounding_rect(50)
		self.rect.center = (rx,ry)
		
		#Set powerup duration in frames
		self.powerup_duration = int(duration)
		
	def shrink(self, duration):
		#Reset for clean powerup slate
		self.reset()
	
		#Load newer, larger image
		(rx,ry) = self.rect.center
		self.image = pygame.image.load('images/paddle_shrink.png')
		self.orig_image = self.image
		self.rect = self.image.get_bounding_rect(50)
		self.rect.center = (rx,ry)
		
		#Set powerup duration in frames
		self.powerup_duration = int(duration)
		
	def speedUp(self, duration):
		#Reset for clean powerup slate
		self.reset()
		
		#Increase speed
		self.vel_offset += .5
		
		#Set powerup duration in frames
		self.powerup_duration = int(duration)
		
	def speedDown(self, duration):
		#Reset for clean powerup slate
		self.reset()
		
		#Increase speed
		self.vel_offset -= .15
		
		#Set powerup duration in frames
		self.powerup_duration = int(duration)
		
	#Resets paddle to initial appearance
	def reset(self):
		self.vel_offset = 2.5
	
		(rx,ry) = self.rect.center
		self.image = pygame.image.load('images/paddle.png')
		self.orig_image = self.image
		self.rect = self.image.get_bounding_rect(50)
		self.rect.center = (rx,ry)
		
		self.powerup_duration = 0
		
	#resets velocity angle and centers paddle
	def restart(self):
		if self.player == 1:
			self.angle_offset = -180
		elif self.player == 2:
			self.angle_offset = 0
		self.reset()
		self.rect.centerx = self.gs.size[0]/2
		
'''
	Ball Class
	- Behavior
			- Bounce around
'''
class Ball(pygame.sprite.Sprite):
	def __init__(self, gs=None):
		#General sprite properties
		self.gs = gs
		self.image = pygame.image.load('images/ball.png')
		self.orig_image = self.image
		self.rect = self.image.get_rect(center=(self.gs.size[0]/2, self.gs.size[1]/4))
		
		try:
			self.hit_sound = pygame.mixer.Sound('sounds/hit.wav')
			self.bounce_sound = pygame.mixer.Sound('sounds/bounce.wav')
		except:
			pass
		
		#Localized sprite properties for ball movement
		self.last_hit = 0
		if random.random() > .5:
			self.vel = (0,10)
		else:
			self.vel = (0,-10)
		self.vel_offset = 1.0
		self.invincible = 0
		self.powerup_duration = 0
		
		#Set up looping call to send info to client ball, keep gamestate synced
		if PLAYER_INSTANCE == 1 and not SOLO_PLAY:
			self.lc = LoopingCall(self.sendBallState)
			self.lc.start(10.0/60.0)
					
	def tick(self):
	
		#Decrement powerup duration if necessary
		if self.powerup_duration > 0:
			self.powerup_duration -= 1
		
		#Reset ball properties
		if self.powerup_duration == 1:
			self.reset()

		# check if ball has passed paddle 2, reset game
		if self.rect.center[1] < 10:
			self.gs.players[0].score += 1
			if self.gs.players[0].score >= SCORE_TO_WIN:
				self.gs.game_over(1)
			else:
				self.rect.center = [self.gs.size[0]/2,self.gs.size[1]/2]
				self.gs.restart()

		# check if ball has passed paddle 1, reset game
		if self.rect.center[1] > self.gs.size[1]-10:
			self.gs.players[1].score += 1
			if self.gs.players[1].score >= SCORE_TO_WIN:
				self.gs.game_over(2)
			else:
				self.rect.center = [self.gs.size[0]/2,self.gs.size[1]/2]
				self.gs.restart()
	
		#If the ball was not hit recently, handle collisions
		if self.invincible <= 0:
			
			#Check for collision in all game objects
			for item in self.gs.players:
			
				#If there was a collision between the ball and a player's paddle
				if self.rect.colliderect(item.rect):
					self.last_hit = item.player

					try:
						self.hit_sound.play()
					except: 
						pass
						
					#Reflect vector from paddle's normal vector, get new ball velocity vector
					velx = self.vel[0]-2*(item.normal[0]*self.vel[0]+item.normal[1]*self.vel[1])*item.normal[0]
					vely = self.vel[1]-2*(item.normal[0]*self.vel[0]+item.normal[1]*self.vel[1])*item.normal[1] 
					self.vel = (velx,vely)
				
					#Set invincibility... No collisions for X frames, avoids getting stuck in paddle
					self.invincible = 30
		
		else:
			self.invincible -= 1
				
		#Move the rectangle's center.
		# NOTE: uses self.rect.center to allow for floating point values. Rect.move truncates
		# small movements
		self.rect.center = (self.rect.centerx+self.vel[0],self.rect.centery+self.vel[1])
	
		#Bounce off the walls
		if self.rect.centerx < 0 or self.rect.centerx > self.gs.size[0]:
			self.vel = (-self.vel[0],self.vel[1])
			
			try:
				self.bounce_sound.play()
			except:
				pass
				
		if self.rect.centery < 0 or self.rect.centery > self.gs.size[1]:
			self.vel = (self.vel[0],-self.vel[1])
			
			try:
				self.bounce_sound.play()
			except:
				pass
				
	#Function that takes the ball's vital attributes, 
	#and sends them to the other player via the Event Sender
	def sendBallState(self):
		data = dict()
		data['id'] 					= 'ball'
		data['invincible'] 	= self.invincible
		data['vel'] 				= self.vel
		data['last_hit']		= self.last_hit
		data['center']			= self.rect.center
		data['powerup'] 		= self.powerup_duration
		
		es.sendData(json.dumps(data))
		
	#Speed the ball up powerup
	def speedUp(self, duration):
		#Reset for clean ball object
		self.reset()
		
		#Set new velocity
		self.vel_offset += .5
		self.vel = (self.vel[0]*self.vel_offset,self.vel[1]*self.vel_offset)
		
		#Set powerup duration
		self.powerup_duration = int(duration)
		
	def speedDown(self, duration):
		#Reset for clean ball object
		self.reset()
		
		#Set new velocity
		self.vel_offset -= .15
		self.vel = (self.vel[0]*self.vel_offset,self.vel[1]*self.vel_offset)
		
		#Set powerup duration
		self.powerup_duration = int(duration)

	def reset(self):
		self.vel = (self.vel[0]/float(self.vel_offset),self.vel[1]/float(self.vel_offset))
		self.vel_offset = 1.0
		
	def restart(self):
		self.__init__(self.gs)
		
'''
	Powerup
		- Behavior
				- Pops up and affects a player or the ball
'''
class Powerup(pygame.sprite.Sprite):
	def __init__(self, gs=None, pu=None, px=None):
		#Powerup dict
		# Uses a dict for each type of powerup
		#		Each powerup's dict includes:
		#			image: path to image
		# 		effect: [ball, player, opponent] i.e. ball, hitting player, player that didn't hit powerup
		# 		function: 
		#			duration: 
		powerups = dict()
		powerups['paddleExtend'] = dict()
		powerups['paddleExtend']['image'] 		= 'images/powerup_paddleExtend.png'
		powerups['paddleExtend']['effect'] 		= 'player'
		powerups['paddleExtend']['function'] 	= 'player.extend'
		powerups['paddleExtend']['duration'] 	= 500
		powerups['paddleExtend']['display']		= 'Long paddle!'
		powerups['paddleShrink'] = dict()
		powerups['paddleShrink']['image'] 		= 'images/powerup_paddleShrink.png'
		powerups['paddleShrink']['effect'] 		= 'opponent'
		powerups['paddleShrink']['function'] 	= 'player.shrink'
		powerups['paddleShrink']['duration'] 	= 500
		powerups['paddleShrink']['display']		= 'Short paddle!'
		powerups['ballSpeedUp'] = dict()
		powerups['ballSpeedUp']['image'] 			= 'images/ball_speedUp.png'
		powerups['ballSpeedUp']['effect'] 		= 'ball'
		powerups['ballSpeedUp']['function'] 	= 'ball.speedUp'
		powerups['ballSpeedUp']['duration'] 	= 500
		powerups['ballSpeedUp']['display']		= 'Fast ball!'
		powerups['ballSpeedDown'] = dict()
		powerups['ballSpeedDown']['image'] 			= 'images/ball_speedDown.png'
		powerups['ballSpeedDown']['effect'] 		= 'ball'
		powerups['ballSpeedDown']['function'] 	= 'ball.speedDown'
		powerups['ballSpeedDown']['duration'] 	= 500
		powerups['ballSpeedDown']['display']	= 'Slow ball!'	
		powerups['paddleSpeedUp'] = dict()
		powerups['paddleSpeedUp']['image'] 			= 'images/powerup_paddleFast.png'
		powerups['paddleSpeedUp']['effect'] 		= 'player'
		powerups['paddleSpeedUp']['function'] 	= 'player.speedUp'
		powerups['paddleSpeedUp']['duration'] 	= 500
		powerups['paddleSpeedUp']['display']	= 'Fast paddle!'
		powerups['paddleSpeedDown'] = dict()
		powerups['paddleSpeedDown']['image'] 			= 'images/powerup_paddleSlow.png'
		powerups['paddleSpeedDown']['effect'] 		= 'opponent'
		powerups['paddleSpeedDown']['function'] 	= 'player.speedDown'
		powerups['paddleSpeedDown']['duration'] 	= 500
		powerups['paddleSpeedDown']['display']		= 'Slow paddle!'

		#Choose powerup
		if pu: 
			powerup_key = pu
		else:
			powerup_key = powerups.keys()[int(random.random()*len(powerups.keys()))]
			
		self.powerup = powerups[powerup_key]
		
		#General sprite properties
		self.gs = gs
		self.image = pygame.image.load(self.powerup['image'])
		self.orig_image = self.image
		
		#Place at random position
		if px:
			posx = px
		else: 
			posx = random.randint(50,self.gs.size[0]-50)
		
		self.rect = self.image.get_rect(center=(posx, self.gs.size[1]/2))
		
		#If player 1 (server player), broadcast powerup location to other player
		if PLAYER_INSTANCE == 1 and not SOLO_PLAY:
			data = dict()
			data['id']  	= 'powerup'
			data['type'] 	= powerup_key
			data['posx'] 	= posx
			
			es.sendData(json.dumps(data))
		
	def tick(self):
		#Check for collision with ball
		if self.rect.colliderect(self.gs.ball.rect):
			#Powerup effecting the player, finds player that hit the ball, runs function
			if self.powerup['effect'] == 'player':
				for player in self.gs.players:
					if self.gs.ball.last_hit == player.player:
						exec(self.powerup['function']+'('+str(self.powerup['duration'])+')')
			#Powerup effecting the opposing player, finds player that didn't hit the ball, runs function
			elif self.powerup['effect'] == 'opponent':
				for player in self.gs.players:
					if self.gs.ball.last_hit != player.player:
						exec(self.powerup['function']+'('+str(self.powerup['duration'])+')')
			#Powerup effecting the ball, runs function
			elif self.powerup['effect'] == 'ball':
				exec('self.gs.'+self.powerup['function']+'('+str(self.powerup['duration'])+')')

			# display powerup name
			self.gs.powerup_text = self.powerup['display']
			self.gs.powerup_text_duration = 120

			# play powerup sound
			try:
				self.gs.powerup_sound.play()
			except: 
				pass
				
			#If hit, make blank image, don't allow being hit again
			self.image = pygame.image.load('images/powerup_blank.png')
			self.rect = self.image.get_rect(center=(-50,-50))
			
			#Delete self			
			self.gs.objects.remove(self)
		
	
#Gamespace class for displaying and updating game screen
class GameSpace:
	def __init__(self):
		#Initiation
		pygame.init()
		
		#try to initialize sounds
		try:
			pygame.mixer.init()
		except:
			pass
			
		#Set screen size and black tuples
		self.size = self.width,self.height = 480,640
		self.black = 0,0,0
		
		#Create the screen
		self.screen = pygame.display.set_mode(self.size)
		pygame.display.set_caption('SuperPong')
		
		#Create game objects
		self.clock = pygame.time.Clock()
		self.objects = list()
		self.players = list()
		
		#Add game objects
		self.players.append(Paddle(1,self))
		self.players.append(Paddle(2,self))
		self.ball	= Ball(self)

		# initialize powerup text display
		self.powerup_text = ""
		self.powerup_text_duration = 0

		# initialize powerup sound
		try:
			self.powerup_sound = pygame.mixer.Sound('sounds/powerup.wav')
		except: 
			pass
			
	def game_loop_iterate(self):

		#Allow first player to create powerups
		if PLAYER_INSTANCE == 1 and len(self.objects) < 3 and random.random() > .995:
			self.objects.append(Powerup(self))

		#Grab user events
		for event in pygame.event.get():
			#Quit when exit pressed
			if event.type == QUIT:
				if reactor.running:
					reactor.stop()
				return
			
		#Run all object ticks
		for obj in self.players:
			obj.tick()
		self.ball.tick()
		for obj in self.objects:
			obj.tick()
					
		#Display all objects
		self.screen.fill(self.black)
		for obj in self.players:
			# display scores
			scoreboard = pygame.image.load('images/score.png')
			font = pygame.font.Font(None, 36)
			text = font.render(str(obj.score), 1, (0, 0, 0))
			textpos = text.get_rect()
			textpos.centerx = 25
			if obj.player == 2:
				textpos.centery = 30
				sb_rect = scoreboard.get_rect(center=(25,25))
			else:
				textpos.centery = self.height-20
				sb_rect = scoreboard.get_rect(center=(25,self.height-25))
			self.screen.blit(scoreboard,sb_rect)
			self.screen.blit(text, textpos)
			self.screen.blit(obj.image,obj.rect)

		self.screen.blit(self.ball.image,self.ball.rect)
		for obj in self.objects:
			self.screen.blit(obj.image,obj.rect)

		# display powerup effect
		font = pygame.font.Font(None, 36)
		powerup_text = font.render(self.powerup_text, 1, (255, 255, 255))
		powerup_textpos = powerup_text.get_rect(center=(self.size[0]/2,self.size[1]/2+60))
		if self.powerup_text_duration > 0:
			self.screen.blit(powerup_text, powerup_textpos)
			self.powerup_text_duration -= 1

			
		pygame.display.flip()
		
	def restart(self):
	
		#Display countdown
		for i in [3,2,1]:
			#self.screen.fill(self.black)	
			logo = pygame.image.load('images/logo.png')
			logo_rect = logo.get_rect(center=(self.size[0]/2,self.size[1]/4))
			
			font = pygame.font.Font(None, 42)
			text = font.render(str(i), 1, (255, 255, 255))
			serve_text = font.render("Serving in", 1, (255, 255, 255))
			serve_textpos = serve_text.get_rect(center=(self.size[0]/2, self.size[1]/2))
			textpos = text.get_rect(center=((self.size[0]/2)+80-(40*i), self.size[1]/2+30))
			self.screen.blit(logo,logo_rect)
			self.screen.blit(serve_text, serve_textpos)
			self.screen.blit(text,textpos)
			pygame.display.flip()
			pygame.time.wait(750)
		
		#Reset players	
		for player in self.players:
			player.restart()
		
		#Reset ball
		self.ball.restart()
		
		#Remove powerups
		self.objects = list()

	def game_over(self, winner):
		font = pygame.font.Font(None, 42)
		text = font.render('Player '+str(winner)+' wins!', 1, (255, 255, 255))
		textpos = text.get_rect(center=((self.size[0]/2), self.size[1]/2-30))
		self.screen.blit(text,textpos)
		pygame.display.flip()
		pygame.time.wait(3000)

		if reactor.running:
			reactor.stop()

			
				
if __name__ == '__main__':
	#Start gamespace
	gs = GameSpace()
	
	#Create server or connect as client
	if PLAYER_INSTANCE == 2 and not SOLO_PLAY:
		reactor.connectTCP(SERVER_HOST,SERVER_PORT,Conn2ServerFactory(gs))
	elif PLAYER_INSTANCE == 1 and not SOLO_PLAY:
		reactor.listenTCP(SERVER_PORT,Conn2ClientFactory(gs))
	
	#If playing alone, seed random, start game
	elif SOLO_PLAY:
		random.seed(time.clock())
		#Start game only after both players are ready
		lc = LoopingCall(gs.game_loop_iterate)
		lc.start(1/45.0)
	else:
		print 'Error: Incorrect player identifier'
		exit(1)
		
	#Start reactor
	reactor.run()
	
